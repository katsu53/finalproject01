package com.example.finalproject01.data

import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface APIService {

    @GET("api/1.0/deals?storeID=1&upperPrice=15")

    suspend fun getSteamdealsList(): Response<DataSteamDeals>


}