package com.example.finalproject01.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.example.finalproject01.data.APIService
import com.example.finalproject01.data.DataSteamDealsItem
import com.example.finalproject01.databinding.FragmentHomeBinding
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    val steams = listOf<DataSteamDealsItem>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        val homeViewModel =
            ViewModelProvider(this).get(HomeViewModel::class.java)

        _binding = FragmentHomeBinding.inflate(inflater, container, false)
        val root: View = binding.root

        val rvSteams: RecyclerView = binding.rvSteamDeals

        rvSteams.adapter = HomeAdapter().apply {
            setSteamdealsData(steams)
        }

        val textView: TextView = binding.textHome

        homeViewModel.text.observe(viewLifecycleOwner) {
            textView.text = it

        }







        return root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }


    fun retrofitService(): APIService {
        return Retrofit
            .Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .baseUrl("https://www.cheapshark.com/")
            .build()
            .create(APIService::class.java)
    }
}